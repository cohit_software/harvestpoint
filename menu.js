$('.carusol').slick({
    dots: true,
    autoplay: true,
    arrows: true,
    speed: 300,
    infinite: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false
        }
      }
    ]
  })
  
  $('.slider').slick({
    dots: false,
    infinite: false,
    arrows: false,
    speed: 300,
    slidesToShow: 4,
    autoplay: true,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
     
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false
        }
      }
      
    ]
  });

 const burger = document.querySelector('.burger')
 const closebtn = document.querySelector('.close-btn')
 const nav = document.querySelector('.nav-mob')

 burger.addEventListener ('click', openNav); 

 closebtn.addEventListener('click', closeNav)
function openNav(e) {
    e.preventDefault()
    nav.classList.replace('hidden','flex')
}
function closeNav(e) {
    e.preventDefault()
    nav.classList.replace('flex', 'hidden')
}

let reloader = document.querySelector('.reload')
reloader.onclick = ((e) => scrollToTop(e))
function scrollToTop(e) {
  e.preventDefault()
  window.scrollTo(0, 0)
}












 
 



          