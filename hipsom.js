let fillCon = document.querySelector('.fill-container')
let uploadCon = document.querySelector('.upload-container')
let btnUpload = document.querySelector('.btn-upload')
let btnCon = document.querySelector('.btn-con')
let btnFill = document.querySelector('.btn-fill')


btnCon.addEventListener('click', (e) => {
  e.preventDefault();
    if(e.target.classList.contains('btn-upload')) {
      if (uploadCon.classList.contains('hidden')) {
        uploadCon.classList.replace('hidden', 'block')
        fillCon.classList.replace('block', 'hidden')
      }
      else {
        false
      }
    } else if (e.target.classList.contains('btn-fill')) {
      if (fillCon.classList.contains('hidden')) {
        fillCon.classList.replace('hidden', 'block')
        uploadCon.classList.replace('block','hidden')
      }
      else {
        false
      }
    }
})