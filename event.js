
  
let cardHolder = document.querySelector('.card-holder')
let eventImg = document.querySelector('.event-image')
let popUp = document.querySelector('.pop-up')
let contentHolder = document.querySelector('.content-holder')
let popImg = document.querySelector('.pop-img')
let popText = document.querySelector('.pop-text')
let pText = document.querySelector('.pop_text')
let eventClose = document.querySelector('.event-close')


cardHolder.addEventListener('click', (e) => {
  if (e.target.classList.contains('event-image')) {
    let imgSrc = e.target.getAttribute('src');
    let modalTitle = e.target.parentElement.children[1].innerText;
    let modalDate = e.target.parentElement.children[2].innerText;
    contentHolder.classList.replace('hidden','block' )
    popImg.src = imgSrc;
    popText.innerText = modalTitle;
    pText.innerText = modalDate;
  }
})
contentHolder.addEventListener('click', (e) => {
  if (e.target.classList.contains('block')) {
    contentHolder.classList.replace('block', 'hidden')
  }
})
eventClose.addEventListener('click', (e) => {
  contentHolder.classList.replace('block', 'hidden')
})